# Conway's approach to Symmetry

**Stage**                      |**time**
------------------------------:|:------
intro                          | 0:00
Symmetry Features              |  0:05
Orbifold examples              |  0:15
Orbifold Euler char            |  0:24
Recreate from signature        |  0:29
Spherical example              |  0:35
Back to wallpaper grps         |  0:41
Reptile + Heesch               |  0:44
Isohedral                      |  0:55
