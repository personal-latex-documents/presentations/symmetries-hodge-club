# Conway's Approach to Symmetry

## Context

[Hodge Club](https://hodge.maths.ed.ac.uk/tiki/Hodge+Club)
talk at the University of Edinburgh on the 22nd of April 2022

## PDF Links

Latest build on main branch of gitlab repo:
[pdf](https://git.ecdf.ed.ac.uk/personal-latex-documents/presentations/symmetries-hodge-club/-/jobs/artifacts/main/raw/presentation.pdf?job=build)

## Abstract

When I first saw the classification of wallpaper groups, as I'm sure it was the
same with many others, it was a very algebraic approach. For example
considering the translation subgroup, the point group, and ways they can fit
together. This past semester I had the opportunity to tutor for Toby Bailey's
course "Symmetry and Geometry", which is based on material from a book
co-authored by one of his past lecturers John Conway titled "The Symmetries of
things". The approach in this text is, instead of considering the groups
themselves, to consider the space (which has the symmetries) quotiented by the
group of symmetries giving so called orbifolds. This method generalises well to
spherical and hyperbolic symmetry and gives clean picturesque ways of
classifying certain types of tilings. This talk is a 50 minute picturesque
summary and advert for the topics in this area.
